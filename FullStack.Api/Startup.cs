﻿using FullStack.Api.Config;
using FullStack.Api.Extensions;
using FullStack.Api.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FullStack.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddConfig(_configuration);
            services.AddAuth(_configuration.GetSection("AuthConfig").Get<AuthConfig>());
            services.AddSignalRCustom(_configuration.GetConnectionString("AzureSignalR"));
            services.AddCosmosDB(_configuration.GetSection("CosmosDBConfig").Get<CosmosDBConfig>());
            services.AddCloudQueueClient(_configuration.GetConnectionString("AzureStorage"));
            services.AddRepositories();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();

            app.UseAzureSignalR(routes =>
            {
                routes.MapHub<PokeHub>("/api/pokehub");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
