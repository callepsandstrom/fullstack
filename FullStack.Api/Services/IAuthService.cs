﻿namespace FullStack.Api.Services
{
    public interface IAuthService
    {
        string CreateAccessToken(string userId);

        byte[] CreateSalt();

        string HashPassword(string password, byte[] salt);

    }
}
