﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace FullStack.Api.Hubs
{
    [Authorize]
    public class PokeHub : Hub { }
}
