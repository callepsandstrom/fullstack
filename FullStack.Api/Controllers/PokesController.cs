﻿using FullStack.Api.Models;
using FullStack.Domain;
using FullStack.Domain.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace FullStack.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PokesController : ControllerBase
    {
        private readonly CloudQueueClient _queueClient;
        private readonly IUserRepository _userRepository;
        private readonly IPokeRepository _pokeRepository;

        public PokesController(CloudQueueClient queueClient, IUserRepository userRepository, IPokeRepository pokeRepository)
        {
            _queueClient = queueClient;
            _userRepository = userRepository;
            _pokeRepository = pokeRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var pokes = await _pokeRepository.GetPokesAsync();

            return Ok(pokes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PokeModel model)
        {
            var userId = User.Identity.Name;
            var sender = await _userRepository.GetUserByIdAsync(userId);

            if (sender == null)
            {
                return BadRequest();
            }

            var poke = new Poke(sender.Id, sender.Username, model.ReceiverId, model.ReceiverUsername, DateTime.Now);
            var content = JsonConvert.SerializeObject(poke);

            await _queueClient
                .GetQueueReference("pokes")
                .AddMessageAsync(new CloudQueueMessage(content));

            return Ok();
        }
    }
}
