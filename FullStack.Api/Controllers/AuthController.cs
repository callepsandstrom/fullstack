﻿using FullStack.Api.Models;
using FullStack.Api.Services;
using FullStack.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FullStack.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IUserRepository _userRepository;

        public AuthController(IAuthService authService, IUserRepository userRepository)
        {
            _authService = authService;
            _userRepository = userRepository;
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn([FromBody]SignInModel model)
        {
            var user = await _userRepository.GetUserByUsernameAsync(model.Username);

            if (user == null)
            {
                return BadRequest("Username or password is incorrect.");
            }

            var passwordHash = _authService.HashPassword(model.Password, user.Salt);

            if (user.PasswordHash != passwordHash)
            {
                return BadRequest("Username or password is incorrect.");
            }

            return Ok(new
            {
                AccessToken = _authService.CreateAccessToken(user.Id),
                UserId = user.Id
            });
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            if (model.Password != model.ConfirmPassword)
            {
                return BadRequest("Password does not match the confirm password.");
            }

            var salt = _authService.CreateSalt();
            var passwordHash = _authService.HashPassword(model.Password, salt);
            var response = await _userRepository.CreateUserAsync(model.Username, passwordHash, salt);

            return Ok(new
            {
                AccessToken = _authService.CreateAccessToken(response.Resource.Id),
                UserId = response.Resource.Id
            });
        }
    }
}
