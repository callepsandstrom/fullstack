﻿using Microsoft.AspNetCore.Mvc;

namespace FullStack.Api.Controllers
{
    [Route("")]
    [ApiController]
    public class RootController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Eyoooooooooow!");
        }
    }
}
