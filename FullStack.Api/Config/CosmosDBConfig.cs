﻿namespace FullStack.Api.Config
{
    public class CosmosDBConfig
    {
        public string Endpoint { get; set; }
        public string AccountKey { get; set; }
    }
}
