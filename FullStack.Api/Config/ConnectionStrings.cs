﻿namespace FullStack.Api.Config
{
    public class ConnectionStrings
    {
        public string AzureStorage { get; set; }
        public string AzureSignalR { get; set; }
    }
}
