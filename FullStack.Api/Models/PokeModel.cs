﻿namespace FullStack.Api.Models
{
    public class PokeModel
    {
        public string ReceiverId { get; }
        public string ReceiverUsername { get; }

        public PokeModel(string receiverId, string receiverUsername)
        {
            ReceiverId = receiverId;
            ReceiverUsername = receiverUsername;
        }
    }
}
