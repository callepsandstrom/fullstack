﻿namespace FullStack.Api.Models
{
    public class SignInModel
    {
        public string Username { get; }
        public string Password { get; }

        public SignInModel(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }

    public class RegisterModel
    {
        public string Username { get; }
        public string Password { get; }
        public string ConfirmPassword { get; }

        public RegisterModel(string username, string password, string confirmPassword)
        {
            Username = username;
            Password = password;
            ConfirmPassword = confirmPassword;
        }
    }
}
