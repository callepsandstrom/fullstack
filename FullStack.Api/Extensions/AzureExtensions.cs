﻿using FullStack.Api.Config;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;

namespace FullStack.Api.Extensions
{
    public static class AzureExtensions
    {
        public static IServiceCollection AddSignalRCustom(this IServiceCollection services, string connectionString)
        {
            services.AddSignalR().AddAzureSignalR(connectionString);

            return services;
        }

        public static IServiceCollection AddCloudQueueClient(this IServiceCollection services, string connectionString)
        {
            var client = CloudStorageAccount.Parse(connectionString).CreateCloudQueueClient();
            var pokesQueue = client.GetQueueReference("pokes");

            pokesQueue.CreateIfNotExists();
            services.AddSingleton(client);

            return services;
        }

        public static IServiceCollection AddCosmosDB(this IServiceCollection services, CosmosDBConfig config)
        {
            var databaseId = "fullstackdb";
            var client = new DocumentClient(new Uri(config.Endpoint), config.AccountKey);

            client.CreateDatabaseIfNotExistsAsync(new Database { Id = databaseId }).Wait();
            client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), new DocumentCollection { Id = "users" }).Wait();
            client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), new DocumentCollection { Id = "stuff" }).Wait();
            client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), new DocumentCollection { Id = "leases" }).Wait();

            services.AddSingleton(new DocumentClient(new Uri(config.Endpoint), config.AccountKey));

            return services;
        }
    }
}
