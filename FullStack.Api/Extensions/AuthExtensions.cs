﻿using FullStack.Api.Config;
using FullStack.Api.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FullStack.Api.Extensions
{
    public static class AuthExtensions
    {
        public static IServiceCollection AddAuth(this IServiceCollection services, AuthConfig config)
        {
            services
                .AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = true;
                    o.TokenValidationParameters = CreateTokenValidationParameters(config);
                });

            services.AddTransient<IAuthService, AuthService>();

            return services;
        }

        private static TokenValidationParameters CreateTokenValidationParameters(AuthConfig config) => new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = CreateSymmetricSecurityKey(config.Secret),
            ValidateIssuer = false,
            ValidateAudience = false
        };

        private static SymmetricSecurityKey CreateSymmetricSecurityKey(string secret) => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));
    }
}
