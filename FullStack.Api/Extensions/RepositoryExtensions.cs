﻿using FullStack.Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace FullStack.Api.Extensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPokeRepository, PokeRepository>();

            return services;
        }
    }
}
