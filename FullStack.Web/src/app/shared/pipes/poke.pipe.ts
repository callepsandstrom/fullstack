import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'poke'
})
export class PokePipe implements PipeTransform {

  transform(poke: any): any {
    return `${poke.senderUsername} sent a poke to ${poke.receiverUsername}`;
  }

}
