import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatInputModule } from '@angular/material';
import { PokePipe } from './pipes/poke.pipe';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule
  ],
  declarations: [
    PokePipe
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    PokePipe
  ]
})
export class SharedModule { }
