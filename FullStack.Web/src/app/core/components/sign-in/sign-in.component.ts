import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SignIn } from '@fs/core/store/auth/auth.state';
import { Navigate } from '@ngxs/router-plugin';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  form: FormGroup;

  constructor(private actions: Actions, private formBuilder: FormBuilder, private store: Store) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

    this.actions
      .pipe(ofActionSuccessful(SignIn))
      .subscribe(() => this.store.dispatch(new Navigate(['/home'])));
  }

  onSubmit(): void {
    this.store.dispatch(new SignIn(this.form.value));
  }
}

