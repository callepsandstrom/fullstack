import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Register } from '@fs/core/store/auth/auth.state';
import { Navigate } from '@ngxs/router-plugin';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(private actions: Actions, private formBuilder: FormBuilder, private store: Store) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      confirmPassword: [null, Validators.required],
    });

    this.actions
      .pipe(ofActionSuccessful(Register))
      .subscribe(() => this.store.dispatch(new Navigate(['/home'])));
  }

  onSubmit(): void {
    this.store.dispatch(new Register(this.form.value));
  }

}
