import { HttpClient } from '@angular/common/http';
import { environment } from '@fs/environment';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

export class SignIn {
  static readonly type = '[Auth] Sign In';

  constructor(public payload: { username: string, password: string }) { }
}

export class Register {
  static readonly type = '[Auth] Register';

  constructor(public payload: { username: string, password: string, confirmPassword: string }) { }
}

export class SignOut {
  static readonly type = '[Auth] Sign Out';
}

export interface AuthStateModel {
  error: any;
  accessToken: string;
  username: string;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    error: undefined,
    accessToken: undefined,
    username: undefined
  }
})
export class AuthState {

  subscription: Subscription;

  constructor(private http: HttpClient) { }

  @Selector()
  static accessToken(state: AuthStateModel) {
    return state.accessToken;
  }

  @Selector()
  static authenticated(state: AuthStateModel) {
    return !!state.accessToken;
  }

  @Action(SignIn)
  signIn(context: StateContext<AuthStateModel>, action: SignIn) {
    return this.http.post<any>(`${environment.apiUrl}/auth/sign-in`, action.payload).pipe(
      tap(response => context.patchState({
        accessToken: response.accessToken,
        username: action.payload.username
      }))
    );
  }

  @Action(Register)
  register(context: StateContext<AuthStateModel>, action: Register) {
    return this.http.post<any>(`${environment.apiUrl}/auth/register`, action.payload).pipe(
      tap(response => context.patchState({
        accessToken: response.accessToken,
        username: action.payload.username
      }))
    );
  }

  @Action(SignOut)
  signOut(context: StateContext<AuthStateModel>) {
    context.patchState({ accessToken: null });
  }

}
