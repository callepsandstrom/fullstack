import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthState } from '@fs/store/auth/auth.state';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private store: Store) { }

  canActivate(): boolean {
    const authenticated = this.store.selectSnapshot(AuthState.authenticated);

    if (!authenticated) {
      this.store.dispatch(new Navigate(['sign-in']));
      return false;
    }

    return true;
  }

}
