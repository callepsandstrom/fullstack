import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SignInComponent } from '@fs/core/components/sign-in/sign-in.component';
import { AuthState } from '@fs/core/store/auth/auth.state';
import { SharedModule } from '@fs/shared/shared.module';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { RegisterComponent } from './components/register/register.component';

@NgModule({
  imports: [
    CommonModule,
    NgxsModule.forRoot([
      AuthState,
    ]),
    NgxsStoragePluginModule.forRoot({
      key: 'auth.accessToken'
    }),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    ReactiveFormsModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    SignInComponent,
    RegisterComponent,
  ],
})
export class CoreModule { }
