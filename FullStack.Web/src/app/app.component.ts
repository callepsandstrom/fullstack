import { Component, OnInit } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { SignOut } from './core/store/auth/auth.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private actions: Actions, private store: Store) { }

  ngOnInit(): void {
    this.actions.pipe(ofActionDispatched(SignOut)).subscribe(() => this.store.dispatch(new Navigate(['/sign-in'])));
  }
}
