import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  @Input() users: any[];

  @Output() poked = new EventEmitter<any>();

  poke(user: any): void {
    this.poked.emit(user);
  }
}
