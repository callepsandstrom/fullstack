import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { AuthState } from '@fs/core/store/auth/auth.state';
import { environment } from '@fs/environment';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

export class Init {
  static readonly type = '[Home] Init';
}

export class Poke {
  static readonly type = '[Home] Poke';

  constructor(public payload: { userId: string, username: string }) { }
}

export class Subscribe {
  static readonly type = '[Home] Subscribe';
}

export class Unsubscribe {
  static readonly type = '[Home] Unsubscribe';
}

export interface HomeStateModel {
  pokes: any[];
  users: any[];
}

@State<HomeStateModel>({
  name: 'home',
  defaults: {
    pokes: [],
    users: []
  }
})
export class HomeState {

  subscription: Subscription;

  constructor(private http: HttpClient, private store: Store) { }

  @Selector()
  static pokes(state: HomeStateModel) {
    return state.pokes;
  }

  @Selector()
  static users(state: HomeStateModel) {
    return state.users;
  }

  @Action(Init)
  async init(context: StateContext<HomeStateModel>) {
    const accessToken = this.store.selectSnapshot(AuthState.accessToken);
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);

    const pokes = await this.http.get<any[]>(`${environment.apiUrl}/pokes`, { headers }).toPromise();
    const users = await this.http.get<any[]>(`${environment.apiUrl}/users`, { headers }).toPromise();

    context.patchState({ pokes, users });
  }

  @Action(Poke)
  poke(context: StateContext<HomeStateModel>, action: Poke) {
    const accessToken = this.store.selectSnapshot(AuthState.accessToken);
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);

    return this.http.post(`${environment.apiUrl}/pokes`, {
      receiverId: action.payload.userId,
      receiverUsername: action.payload.username
    }, { headers });
  }

  @Action(Subscribe)
  subscribe(context: StateContext<HomeStateModel>) {
    const accessToken = this.store.selectSnapshot(AuthState.accessToken);

    this.subscription = this.onBroadcast<any>('poke', accessToken).subscribe(poke =>
      context.patchState({ pokes: [...context.getState().pokes, poke] })
    );
  }

  @Action(Unsubscribe)
  unsubscribe() {
    this.subscription.unsubscribe();
  }

  private onBroadcast<T>(target: string, accessToken: string): Observable<T> {
    const connection = this.createConnection(accessToken);

    connection.start()
      .then(() => console.log('Connection started'))
      .catch(error => console.error(error));

    return new Observable(subscriber => connection.on(target, payload => subscriber.next(payload)));
  }

  private createConnection(accessToken: string): HubConnection {
    return new HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/pokehub`, { accessTokenFactory: () => accessToken })
      .build();
  }

}
