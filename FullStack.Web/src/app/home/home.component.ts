import { Component, OnDestroy, OnInit } from '@angular/core';
import { SignOut } from '@fs/core/store/auth/auth.state';
import { HomeState, Init, Poke, Subscribe, Unsubscribe } from '@fs/home/home.component.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  @Select(HomeState.pokes) pokes$: Observable<any[]>;
  @Select(HomeState.users) users$: Observable<any[]>;

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch([new Init(), new Subscribe()]);
  }

  ngOnDestroy() {
    this.store.dispatch(new Unsubscribe());
  }

  signOut(): void {
    this.store.dispatch(new SignOut());
  }

  onPoked(user: any): void {
    this.store.dispatch(new Poke({ userId: user.userId, username: user.username }));
  }

}
