import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from '@fs/home/home.component';
import { SharedModule } from '@fs/shared/shared.module';
import { NgxsModule } from '@ngxs/store';
import { FeedComponent } from './feed/feed.component';
import { HomeState } from './home.component.state';
import { UserListComponent } from './user-list/user-list.component';

const routes: Route[] = [{
  path: '', component: HomeComponent
}];

@NgModule({
  declarations: [HomeComponent, UserListComponent, FeedComponent],
  imports: [
    CommonModule,
    NgxsModule.forFeature([
      HomeState
    ]),
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class HomeModule { }
