﻿using Newtonsoft.Json;

namespace FullStack.Domain
{
    public class User
    {
        [JsonProperty("id")]
        public string Id { get; }

        [JsonProperty("username")]
        public string Username { get; }

        [JsonProperty("passwordHash")]
        public string PasswordHash { get; }

        [JsonProperty("salt")]
        public byte[] Salt { get; }

        public User(string id, string username, string passwordHash, byte[] salt)
        {
            Id = id;
            Username = username;
            PasswordHash = passwordHash;
            Salt = salt;
        }
    }
}
