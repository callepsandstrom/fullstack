﻿using FullStack.Domain.Helpers;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullStack.Domain.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DocumentClient _client;

        public UserRepository(DocumentClient client) => _client = client;

        public async Task<ResourceResponse<Document>> CreateUserAsync(string username, string passwordHash, byte[] salt) => await _client
            .CreateDocumentAsync(UriHelper.UserCollectionUri, new { username, passwordHash, salt });

        public async Task<User> GetUserByIdAsync(string id) => (await _client
            .CreateDocumentQuery<User>(UriHelper.UserCollectionUri)
            .Where(x => x.Id == id)
            .AsDocumentQuery()
            .ExecuteNextAsync())
            .FirstOrDefault();

        public async Task<IEnumerable<User>> GetUsersAsync() => (await _client
            .CreateDocumentQuery<User>(UriHelper.UserCollectionUri)
            .AsDocumentQuery()
            .ExecuteNextAsync<User>())
            .ToList();

        public async Task<User> GetUserByUsernameAsync(string username) => (await _client
            .CreateDocumentQuery<User>(UriHelper.UserCollectionUri)
            .Where(x => x.Username == username)
            .AsDocumentQuery()
            .ExecuteNextAsync())
            .FirstOrDefault();
    }
}
