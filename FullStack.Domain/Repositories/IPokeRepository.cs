﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FullStack.Domain.Repositories
{
    public interface IPokeRepository
    {
        Task<IEnumerable<Poke>> GetPokesAsync();
    }
}
