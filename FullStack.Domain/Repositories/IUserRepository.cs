﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FullStack.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUserByUsernameAsync(string username);

        Task<User> GetUserByIdAsync(string id);

        Task<IEnumerable<User>> GetUsersAsync();

        Task<ResourceResponse<Document>> CreateUserAsync(string username, string password, byte[] salt);
    }
}
