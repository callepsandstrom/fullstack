﻿using FullStack.Domain.Helpers;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullStack.Domain.Repositories
{
    public class PokeRepository : IPokeRepository
    {
        private readonly DocumentClient _client;

        public PokeRepository(DocumentClient client) => _client = client;

        public async Task<IEnumerable<Poke>> GetPokesAsync() => (await _client
            .CreateDocumentQuery<Poke>(UriHelper.StuffCollectionUri)
            .Take(20)
            .AsDocumentQuery()
            .ExecuteNextAsync<Poke>())
            .ToList();
    }
}
