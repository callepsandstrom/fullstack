﻿using Newtonsoft.Json;
using System;

namespace FullStack.Domain
{
    public class Poke
    {
        [JsonProperty("senderId")]
        public string SenderId { get; }

        [JsonProperty("senderUsername")]
        public string SenderUsername { get; }

        [JsonProperty("receiverId")]
        public string ReceiverId { get; }

        [JsonProperty("receiverUsername")]
        public string ReceiverUsername { get; }

        [JsonProperty("dateTime")]
        public DateTime DateTime { get; }

        public Poke(string senderId, string senderUsername, string receiverId, string receiverUsername, DateTime dateTime)
        {
            SenderId = senderId;
            SenderUsername = senderUsername;
            ReceiverId = receiverId;
            ReceiverUsername = receiverUsername;
            DateTime = dateTime;
        }
    }
}
