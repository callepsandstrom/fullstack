﻿using Microsoft.Azure.Documents.Client;
using System;

namespace FullStack.Domain.Helpers
{
    public static class UriHelper
    {
        public static Uri StuffCollectionUri => UriFactory.CreateDocumentCollectionUri("fullstackdb", "stuff");

        public static Uri CreateStuffDocumentUri(string id) => UriFactory.CreateDocumentUri("fullstackdb", "stuff", id);

        public static Uri UserCollectionUri => UriFactory.CreateDocumentCollectionUri("fullstackdb", "users");

        public static Uri CreateUserDocumentUri(string id) => UriFactory.CreateDocumentUri("fullstackdb", "users", id);
    }
}
