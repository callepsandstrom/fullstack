﻿using FullStack.Domain;
using FullStack.Domain.Helpers;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FullStack.Functions
{
    public static class ProcessPoke
    {
        [FunctionName("process-poke")]
        public static async Task Run([QueueTrigger("pokes")]Poke poke, [CosmosDB(ConnectionStringSetting = "CosmosDBConnectionString")]DocumentClient client, ILogger log)
        {
            log.LogInformation($"Processing poke from {poke.SenderUsername} to {poke.ReceiverUsername}");

            await client.CreateDocumentAsync(UriHelper.StuffCollectionUri, poke);
        }
    }
}
