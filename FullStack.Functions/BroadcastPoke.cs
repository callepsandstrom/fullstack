﻿using FullStack.Domain;
using FullStack.Domain.Helpers;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FullStack.Functions
{
    public static class BroadcastPoke
    {
        [FunctionName("broadcast-poke")]
        public static async Task Run([CosmosDBTrigger("fullstackdb", "stuff", ConnectionStringSetting = "CosmosDBConnectionString")]IReadOnlyList<Document> pokes, [CosmosDB(ConnectionStringSetting = "CosmosDBConnectionString")]DocumentClient client, [SignalR(HubName = "pokehub")] IAsyncCollector<SignalRMessage> signalRMessages, ILogger log)
        {
            foreach (var poke in pokes)
            {
                var response = await client.ReadDocumentAsync<Poke>(UriHelper.CreateStuffDocumentUri(poke.Id));

                await signalRMessages.AddAsync(new SignalRMessage
                {
                    Target = "poke",
                    Arguments = new[] { response.Document }
                });
            }
        }
    }
}
